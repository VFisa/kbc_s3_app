"__authors__ = 'Martin Fiser, Leo Chan'"
"__credits__ = 'Keboola 2017, Twitter: @VFisa'"

"""
Python 3 environment (unicode script fixes in place)
Boto docs: http://boto.cloudhackers.com
"""


import os
import io
import sys
import requests
import logging
import json
import gzip

import pip 
pip.main(['install', '--disable-pip-version-check', '--no-cache-dir', 'dateparser'])
pip.main(['install', '--disable-pip-version-check', '--no-cache-dir', 'pygelf'])
pip.main(['install', '--disable-pip-version-check', '--no-cache-dir', 'boto'])

from pygelf import GelfTcpHandler
import csv
import time
from keboola import docker
import boto
from boto.s3.key import Key
from boto.exception import S3ResponseError
import datetime
import dateparser
import pandas as pd


# Environment setup
abspath = os.path.abspath(__file__)
script_path = os.path.dirname(abspath)
os.chdir(script_path)


#logging.basicConfig(level=logging.INFO,format='%(asctime)s - %(levelname)s - %(message)s',datefmt="%Y-%m-%d %H:%M:%S")

# initialize application
cfg = docker.Config('/data/')

# Access the supplied parameters
params = cfg.get_parameters()
KEY_ID = cfg.get_parameters()["keyId"]
SKEY_ID = cfg.get_parameters()["#secret_key"]
BUCKET = cfg.get_parameters()["bucket_source"]
ABBREV = cfg.get_parameters()["area"] #folder_string
FILE_NAME = cfg.get_parameters()["file_name"] #destination
#RANGE = cfg.get_parameters()["date_range"]
START_DATE = cfg.get_parameters()["start_date"]
END_DATE = cfg.get_parameters()["end_date"]
DEBUG = cfg.get_parameters()["debug"]
# in production, put this into parameter window:
"""
OLD config
{
    "keyId": "",
    "#secret_key": "",
    "bucket_source": "",
    "area": "",
    "date_range":[""],
    "debug": false
}
NEW config
{
    "keyId": "",
    "#secret_key": "",
    "bucket_source": "",
    "area": "",
    "file_name": "", 
    "start_date":"",
    "end_date":"",
    "debug": false
}
"""

# Logging
logger = logging.getLogger()
fields = {"_some": {"structured": "data"}}
if DEBUG==True:
    logging.basicConfig(
        level=logging.DEBUG,
        format='%(asctime)s - %(levelname)s - %(message)s',
        datefmt="%Y-%m-%d %H:%M:%S"
        )
    """
    logger.addHandler(GelfTcpHandler(
        host=os.getenv('KBC_LOGGER_ADDR'),
        port=os.getenv('KBC_LOGGER_PORT'),
        debug=True, **fields
        ))
    """
    
else:
    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s - %(levelname)s - %(message)s',
        datefmt="%Y-%m-%d %H:%M:%S"
        )
    """
    logger.addHandler(GelfTcpHandler(
        host=os.getenv('KBC_LOGGER_ADDR'),
        port=os.getenv('KBC_LOGGER_PORT'),
        debug=False, **fields
        ))
    """


class sfile:
    """
    represents files on S3
    """

    def __init__(self, name, size, modified, key):
        self.name = name
        self.size = size
        self.modified = modified
        self.key = key



def move(parameter_list):
    """
    Move files
    """

    srcBucket = conn.get_bucket('mybucket001', validate=False)   #Source Bucket Object
    dstBucket = conn.get_bucket('mybucket002', validate=False)   #Destination Bucket Object
    fileName = "abc.txt"
    #Call the copy_key() from destination bucket
    dstBucket.copy_key(fileName,srcBucket.name,fileName)


def download_file(bucket_name, file_name):
    """
    Download single file.
    Relying on not using "." in the filenames!
    In the case we extract CSV files, we can output them (DEFAULT_TABLE_FOLDER)
    ,else lets save them in file storage (DEFAULT_FILE_FOLDER)
    and parse them with something
    """

    # figuring out files, names and extensions
    destFileName = str(file_name.split("/")[-1])
    extension = str(destFileName).split(".")[-1]

    if str(extension) == "csv":
        destFileLocation = str(DEFAULT_TABLE_FOLDER)+str(destFileName)
    if str(extension) == "gz":
        destFileLocation = str(DEFAULT_TABLE_FOLDER)+str(destination)+"/"+str(destFileName)
    else:
        destFileLocation = str(DEFAULT_FILE_FOLDER)+str(destFileName)

    #print(destFileLocation)
    """
    print(extension)
    print(destFileLocation)
    """

    bucket = conn.get_bucket(bucket_name, validate=False)

    #Get the Key object of the given key, in the bucket
    k = Key(bucket, file_name)

    # Get the contents of the key into a file
    try:
        k.get_contents_to_filename(destFileLocation) 
        #temp = k.get_contents_to_string()
        #print (k.get_contents_to_string)
        #k.get_file(all_in_one)

        #with gzip.open(all_in_one, 'wb') as f:
        #    f.write(temp)
        
        logging.info("File "+str(file_name)+" downloaded.")
    except Exception as a:
        logging.error("Could not download "+str(file_name))



def upload_file(bucket_name, file_name):
    """
    Upload single file to the storage
    Not finished yet.
    """
    
    fileName="abcd.txt"
    bucketName="mybucket001"

    file = open(fileName)

    bucket = conn.get_bucket(bucketName, validate=False)
    #Get the Key object of the bucket
    k = Key(bucket)
    #Crete a new key with id as the name of the file
    k.key=file_name
    #Upload the file
    result = k.set_contents_from_file(file)
    #result contains the size of the file uploaded

    return result





def empty_bucket(bucket_name):
    """
    Clear the content of the bucket.
    """

    bucket = conn.get_bucket(bucket_name, validate=False)

    for i in bucket.list():
        print(i.key)
        i.delete() #Delete the object

    pass


def list_items(bucket_name, folder=""):
    """
    list items in the bucket.
    returns a list of objects
    
    supported info:
        .name
        .key
        .size
        .last_modified
    
    if nonexistent is None:
        print "No such bucket!"
    """

    # test if bucket is available
    buckets_available = list_buckets()
    if bucket_name in buckets_available:
        logging.info("Bucket found on S3: "+bucket_name)
    else:
        logging.error("Bucket is not present on S3. Exit!")
        sys.exit(1)

    bucket = conn.get_bucket(bucket_name, validate=False)
    items = []

    for i in bucket.list(prefix=folder):
        name = i.name
        location = i.key
        size = i.size
        modified = i.last_modified

        info = sfile(name, size, modified, location)
        items.append(info)

    return items


def list_buckets():
    """
    List buckets and their properties
    """

    try:
        buckets = conn.get_all_buckets()
    except (OSError,S3ResponseError) as e:
        logging.error("Could not list buckets. Please check credentials. Exit!")
        sys.exit(0)

    buckets_names = []

    for i in buckets:
        name = i.name
        buckets_names.append(name)

    return buckets_names


def produce_manifest(file_type, file_name):
    """
    Dummy function to return header per file type.
    """

    file = "/data/out/tables/"+str(file_name)+".manifest"
    destination_part = destination.split(".csv")[0]

    manifest_template = {#"source": "myfile.csv",
                         "destination": "in.c-mybucket.table",
                         "columns": [""],
                         "delimiter": "|"
                         #"enclosure": ""
                        }

    if file_type == "SOS":
        header = ["SiteID","UID","ActivityLevel","CourseUID","CourseName","CourseNumber","CoursePaidTime","CourseParkTime","CoursePreparationTime","CourseStartTimeLocal","CourseStartTimeUtc","CourseType","Destination","DestinationName","DisplayGroupID","FastTrackedTime","FirstDisplayTime","FirstStoreTime","FirstTenderTime","FoodDeliveredTime","ItemCategory","ItemCookStartTime","ItemCookTime","ItemDescription","ItemUID","ItemID","ItemNumber","ItemQuantity","ItemTagTime","LastBumpTime","LastPreparedTime","LastRecallTime","LastTotalTime","LastUnBumpTime","Modifier1ID","Modifier2ID","Modifier3ID","ParentItemNumber","PriorityStatusReached","PriorityTime","RemakeTime","RushStatusReached","RushTime","ServerID","ServerName","SOSTag","StationType","Tbl","TerminalNumber","TimestampLocal","TimestampUtc","TopLevelItem","TransactionUID","TransactionNumber","TransactionStartTimeLocal","TransactionStartTimeUtc","ViewID","ViewName","InsertDate","GuestCount","FirstViewedTime"]
    else:
        header = ""

    manifest = manifest_template
    manifest["columns"] = header
    #manifest["source"] = str(file_name)
    manifest["destination"] = "in.c-"+str(bucket_name)+"."+str(destination_part)

    try:
        with open(file, 'w') as file_out:
            json.dump(manifest, file_out)
            logging.info("Output manifest file produced.")
    except Exception as e:
        logging.error("Could not produce output file manifest.")
        logging.error(e)
    print(manifest)
    #return manifest


def create_table_folder(folder_name):
    """
    prep folder in /data for files to be saved
    """

    destination = str(DEFAULT_TABLE_FOLDER)+"/"+str(folder_name)
    #print(destination)

    try:
        os.stat(destination)
    except:
        os.mkdir(destination)  

    pass



def dates_request(start_date, end_date):
    dates = []
    try:
        start_date_form = dateparser.parse(start_date)
        end_date_form = dateparser.parse(end_date)
        day_diff = (end_date_form-start_date_form).days
        if day_diff < 0:
            print("ERROR: start_date cannot exceed end_date.")
            print("Exit.")
            sys.exit(0)
        temp_date = start_date_form
        day_n = 0
        if day_diff == 0:
            dates.append(temp_date.strftime("%Y/%-m/%-d"))
        while day_n < day_diff:
            dates.append(temp_date.strftime("%Y/%-m/%-d"))
            temp_date += datetime.timedelta(days=1)
            day_n += 1
            if day_n == day_diff:
                dates.append(temp_date.strftime("%Y/%-m/%-d"))
    except TypeError:
        print("ERROR: Please enter valid date parameters")
        print("Exit.")
        sys.exit(1)

    return dates


def output_csv(fileinput, fileoutput):
    ## Output the GZ file to CSV
    with gzip.open(fileinput, mode='rb') as files:
        file_content = files.read()
        gzbody = io.BytesIO(file_content)
        got_text = gzip.GzipFile(None, 'rb', fileobj=gzbody).read().decode('utf-8')
        with open(fileoutput,'w') as my_csv:
            my_csv.write(got_text)

def combine_gz(file_output):
    with gzip.open(file_output, 'wb') as f:
        for i in date_list:
            file_structure = str(i)
            dest_string = folder_string+"/"+file_structure
            bucket_items = list_items(bucket_name, dest_string)
            
            number_files = 0

            ##############################################
            for a in bucket_items:
                bucket = conn.get_bucket(bucket_name, validate=False)
                k = Key(bucket,a.name)

                try:
                    #download_file(bucket_name, a.name)
                    #k.get_contents_to_file(f)#,headers=columns_header)
                    k.get_file(f)
                    number_files += 1
                    logging.info("File "+str(a.name)+" downloaded.")
                except Exception as b:
                    logging.error("Could not download "+str(a.name))
                    
            
            ##############################################
            print(str(i)+": "+str(number_files)+" files.")
    f.close()


if __name__ == "__main__":
    """
    Main execution script.
    """

    DEFAULT_FILE_FOLDER = "/data/in/files/"
    DEFAULT_TABLE_FOLDER = "/data/out/tables/"

    # Try out the connection
    try:
        conn = boto.connect_s3(KEY_ID,SKEY_ID)
        logging.info("Successfully connected."+str(conn))
    except Exception as a:
        logging.error("Could not connect. Exit!")
        sys.exit(1)

    #list_buckets()

    # Basic information
    date_list = dates_request(START_DATE,END_DATE)
    
    #file_structure = "2017/6/14"
    #bucket_name = "qsr-playground"
    #folder_string = "test_folder"
    #destination = "sos-company.csv"
    bucket_name = BUCKET
    folder_string = ABBREV
    destination = FILE_NAME
    
    #dest_string = folder_string+"/"+file_structure

    # Preparation for split file
    #create_table_folder(destination)
    #create_table_folder(dest_string)
    produce_manifest("SOS", destination)


    all_in_one_name = destination+".gz"
    all_in_one  = "/data/in/tables/"+all_in_one_name
    all_in_one_csv = "/data/out/tables/"+destination

    #################################
    ### trial 4: combine all gz and output as one csv
    ## combine all GZ files
    combine_gz(all_in_one)
    ## output csv
    output_csv(all_in_one, all_in_one_csv)
    
    logging.info("Script completed.")
